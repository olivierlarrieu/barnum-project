import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import App from './app'
import './main.css'
import 'animate.css'


ReactDOM.render( 
    <App />
,
    document.getElementById('root')
);


registerServiceWorker();
