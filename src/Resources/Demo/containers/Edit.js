import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EditComponent from '../components/Edit';
//actions
import { setName } from "../actions/Edit"

/** the Edit component */
class Edit extends React.Component {

    render() {

        return (

            <EditComponent>
            { this.props.children }
            </EditComponent>
            
        )
    
    }

}

/** the Edit component propTypes for typechecking */
Edit.propTypes = {

}

//-----------------------------------------------------------------------------
/** the Edit component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Demo_Edit.name,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
    }
}


const EditContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(Edit)
//-----------------------------------------------------------------------------

export { Edit, EditContainer }
