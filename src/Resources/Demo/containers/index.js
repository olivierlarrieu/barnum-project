import { DetailContainer } from './Detail'
import { EditContainer } from './Edit'
import { FiltersContainer } from './Filters'
import { ListContainer } from './List'


export {DetailContainer, EditContainer, FiltersContainer, ListContainer}
