import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FiltersComponent from '../components/Filters';
//actions
import { setName } from "../actions/Filters"

/** the Filters component */
class Filters extends React.Component {

    render() {

        return (

            <FiltersComponent>
            { this.props.children }
            </FiltersComponent>
            
        )
    
    }

}

/** the Filters component propTypes for typechecking */
Filters.propTypes = {

}

//-----------------------------------------------------------------------------
/** the Filters component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Demo_Filters.name,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
    }
}


const FiltersContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(Filters)
//-----------------------------------------------------------------------------

export { Filters, FiltersContainer }
