import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DetailComponent from '../components/Detail';
//actions
import { setName } from "../actions/Detail"

/** the Detail component */
class Detail extends React.Component {

    render() {

        return (

            <DetailComponent>
            { this.props.children }
            </DetailComponent>
            
        )
    
    }

}

/** the Detail component propTypes for typechecking */
Detail.propTypes = {

}

//-----------------------------------------------------------------------------
/** the Detail component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Demo_Detail.name,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
    }
}


const DetailContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(Detail)
//-----------------------------------------------------------------------------

export { Detail, DetailContainer }
