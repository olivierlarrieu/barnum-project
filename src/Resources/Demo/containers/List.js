import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListComponent from '../components/List';
//actions
import { setName } from "../actions/List"

/** the List component */
class List extends React.Component {

    render() {

        return (

            <ListComponent>
            { this.props.children }
            </ListComponent>
            
        )
    
    }

}

/** the List component propTypes for typechecking */
List.propTypes = {

}

//-----------------------------------------------------------------------------
/** the List component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Demo_List.name,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
    }
}


const ListContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(List)
//-----------------------------------------------------------------------------

export { List, ListContainer }
