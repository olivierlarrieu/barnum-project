import { setState } from '../libs'

//constants
const SET_RESOURCES_DEMO_FILTERS_NAME = "SET_RESOURCES_DEMO_FILTERS_NAME"


const FiltersState = {
    //props
    name: "Filters",

}

const FiltersReducer = (state = FiltersState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_RESOURCES_DEMO_FILTERS_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { FiltersState, FiltersReducer, SET_RESOURCES_DEMO_FILTERS_NAME }
