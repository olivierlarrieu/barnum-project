import { setState } from '../libs'

//constants
const SET_RESOURCES_DEMO_DETAIL_NAME = "SET_RESOURCES_DEMO_DETAIL_NAME"


const DetailState = {
    //props
    name: "Detail",

}

const DetailReducer = (state = DetailState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_RESOURCES_DEMO_DETAIL_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { DetailState, DetailReducer, SET_RESOURCES_DEMO_DETAIL_NAME }
