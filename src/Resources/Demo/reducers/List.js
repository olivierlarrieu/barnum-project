import { setState } from '../libs'

//constants
const SET_RESOURCES_DEMO_LIST_NAME = "SET_RESOURCES_DEMO_LIST_NAME"


const ListState = {
    //props
    name: "List",

}

const ListReducer = (state = ListState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_RESOURCES_DEMO_LIST_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { ListState, ListReducer, SET_RESOURCES_DEMO_LIST_NAME }
