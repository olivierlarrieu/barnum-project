import { setState } from '../libs'

//constants
const SET_RESOURCES_DEMO_EDIT_NAME = "SET_RESOURCES_DEMO_EDIT_NAME"


const EditState = {
    //props
    name: "Edit",

}

const EditReducer = (state = EditState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_RESOURCES_DEMO_EDIT_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { EditState, EditReducer, SET_RESOURCES_DEMO_EDIT_NAME }
