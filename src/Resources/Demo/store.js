import {  
  combineReducers,
  createStore,
} from 'redux';

//imports
import { ListReducer } from 'Demo/reducers/List'
import { DetailReducer } from 'Demo/reducers/Detail'
import { EditReducer } from 'Demo/reducers/Edit'
import { FiltersReducer } from 'Demo/reducers/Filters'

const RootReducer = {
  //reducers
  Demo_List: ListReducer,
  Demo_Detail: DetailReducer,
  Demo_Edit: EditReducer,
  Demo_Filters: FiltersReducer,
}

const createReducer = (store={}, loadedReducer={}) => {

  store.LOADED_REDUCERS = {...store.LOADED_REDUCERS, ...loadedReducer}
  let newObject = {...RootReducer, ...store.LOADED_REDUCERS}
  let newReducer = combineReducers(
          newObject
  )
  return newReducer
}

const store = createStore(createReducer(), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
store.LOADED_REDUCERS = {}


export { createReducer, RootReducer }
export default store
