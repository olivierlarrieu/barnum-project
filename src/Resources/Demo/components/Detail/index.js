import React from 'react';
import PropTypes from 'prop-types';


/** the Detail component */
class Detail extends React.Component {

    render() {

        return (

            <div className={"DetailComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the Detail component propTypes for typechecking */
Detail.propTypes = {

}

export default Detail
