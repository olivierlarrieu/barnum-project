import React from 'react';
import PropTypes from 'prop-types';


/** the List component */
class List extends React.Component {

    render() {

        return (

            <div className={"ListComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the List component propTypes for typechecking */
List.propTypes = {

}

export default List
