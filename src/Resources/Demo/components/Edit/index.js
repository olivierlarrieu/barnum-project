import React from 'react';
import PropTypes from 'prop-types';


/** the Edit component */
class Edit extends React.Component {

    render() {

        return (

            <div className={"EditComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the Edit component propTypes for typechecking */
Edit.propTypes = {

}

export default Edit
