import React from 'react';
import PropTypes from 'prop-types';


/** the Filters component */
class Filters extends React.Component {

    render() {

        return (

            <div className={"FiltersComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the Filters component propTypes for typechecking */
Filters.propTypes = {

}

export default Filters
