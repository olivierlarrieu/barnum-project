import React from 'react';
import { Switch, Route, Link } from 'react-router-dom'
import Container from './components/Container'
import {DetailContainer, EditContainer, FiltersContainer, ListContainer} from './containers'
import { RootReducer } from './store'

/* simple boilerplate for router */
const ResourceRouter = (props) => {

    switch (props.match.params.sectionName) {

        case "detail":
            return <DetailContainer {...props}> Detail Container </DetailContainer>

        case "edit":
            return <EditContainer {...props}> Edit Container </EditContainer>

        case "new":
            return <EditContainer {...props}> New Container </EditContainer>

        default:
            return <ListContainer {...props}> List Container </ListContainer>

    }

}

class App extends React.Component {

    render() {

        let {match} = this.props

        return (
                
            <Container>
            <FiltersContainer/>
            <h1>{match.url}</h1>
            <Link to={`${match.url}/new`}>Create - </Link>
            <Link to={`${match.url}/123/detail`}>detail - </Link>
            <Link to={`${match.url}/123/edit`}>edit</Link>
            <Container>
            <Switch>

                <Route exact path={`${match.url}/`} render={(props)=> <ResourceRouter {...props}/>}/>
                <Route exact path={`${match.url}/:id/:sectionName`} render={(props)=> <ResourceRouter {...props}/>}/>
                <Route exact path={`${match.url}/:id/:sectionName`} render={(props)=> <ResourceRouter {...props}/>}/>
                <Route exact path={`${match.url}/:sectionName`} render={(props)=> <ResourceRouter {...props}/>}/>

            </Switch>
            </Container>
            </Container>

        )
    }

}


const Lazy = {
    component: App,
    reducer: RootReducer
}
export default Lazy
