import { SET_RESOURCES_DEMO_DETAIL_NAME } from "../../reducers/Detail"

const setName = (value) => {

    return {type: SET_RESOURCES_DEMO_DETAIL_NAME, payload: {name: value }}

}

export { setName }
