import { SET_RESOURCES_DEMO_EDIT_NAME } from "../../reducers/Edit"

const setName = (value) => {

    return {type: SET_RESOURCES_DEMO_EDIT_NAME, payload: {name: value }}

}

export { setName }
