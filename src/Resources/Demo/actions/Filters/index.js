import { SET_RESOURCES_DEMO_FILTERS_NAME } from "../../reducers/Filters"

const setName = (value) => {

    return {type: SET_RESOURCES_DEMO_FILTERS_NAME, payload: {name: value }}

}

export { setName }
