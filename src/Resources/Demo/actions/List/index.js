import { SET_RESOURCES_DEMO_LIST_NAME } from "../../reducers/List"

const setName = (value) => {

    return {type: SET_RESOURCES_DEMO_LIST_NAME, payload: {name: value }}

}

export { setName }
