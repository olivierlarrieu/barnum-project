import {  
  combineReducers,
  createStore,
} from 'redux';

//imports
import { HomeReducer } from 'Home/reducers/Home'

const RootReducer = {
  //reducers
  Home_Home: HomeReducer,
}

const createReducer = (store={}, loadedReducer={}) => {

  store.LOADED_REDUCERS = {...store.LOADED_REDUCERS, ...loadedReducer}
  let newObject = {...RootReducer, ...store.LOADED_REDUCERS}
  let newReducer = combineReducers(
          newObject
  )
  return newReducer
}

const store = createStore(createReducer(), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
store.LOADED_REDUCERS = {}


export { createReducer, RootReducer }
export default store
