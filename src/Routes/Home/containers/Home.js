import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HomeComponent from '../components/Home';
//actions
import { setName } from "../actions/Home"

/** the Home component */
class Home extends React.Component {

    render() {

        return (

            <HomeComponent>
            { this.props.children }
            </HomeComponent>
            
        )
    
    }

}

/** the Home component propTypes for typechecking */
Home.propTypes = {

}

//-----------------------------------------------------------------------------
/** the Home component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Home_Home.name,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
    }
}


const HomeContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(Home)
//-----------------------------------------------------------------------------

export { Home, HomeContainer }
