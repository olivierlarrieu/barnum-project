import { setState } from '../libs'

//constants
const SET_ROUTES_HOME_HOME_NAME = "SET_ROUTES_HOME_HOME_NAME"


const HomeState = {
    //props
    name: "Home",

}

const HomeReducer = (state = HomeState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_ROUTES_HOME_HOME_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { HomeState, HomeReducer, SET_ROUTES_HOME_HOME_NAME }
