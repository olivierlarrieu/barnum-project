import React from 'react';
import PropTypes from 'prop-types';


/** the Container component */
class Container extends React.Component {

    render() {

        return (

            <div className={"ContainerComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the Container component propTypes for typechecking */
Container.propTypes = {

}

export default Container
