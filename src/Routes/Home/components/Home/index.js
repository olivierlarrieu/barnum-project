import React from 'react';
import PropTypes from 'prop-types';


/** the Home component */
class Home extends React.Component {

    render() {

        return (

            <div className={"HomeComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the Home component propTypes for typechecking */
Home.propTypes = {

}

export default Home
