import React from 'react';
import { RootReducer } from './store'
import Container from './components/Container'
import { HomeContainer } from './containers'

class App extends React.Component {

    render() {

        return (
                
            <Container>

                <HomeContainer>
                
                <h1>{ this.props.match.path }</h1>

                </HomeContainer>

            </Container>

        )
    }

}


const Lazy = {
    component: App,
    reducer: RootReducer
}
export default Lazy
