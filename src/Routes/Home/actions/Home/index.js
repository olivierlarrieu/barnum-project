import { SET_ROUTES_HOME_HOME_NAME } from "../../reducers/Home"

const setName = (value) => {

    return {type: SET_ROUTES_HOME_HOME_NAME, payload: {name: value }}

}

export { setName }
