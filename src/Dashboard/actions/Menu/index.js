import { SET_DASHBOARD_MENU_NAME, SET_DASHBOARD_MENU_ISOPEN } from "../../reducers/Menu"

const setName = (value) => {

    return {type: SET_DASHBOARD_MENU_NAME, payload: {name: value }}

}

const setIsOpen = (value) => {

    return {type: SET_DASHBOARD_MENU_ISOPEN, payload: {isOpen: value }}

}

export { setName, setIsOpen }
