import { SET_DASHBOARD_PAGECONTENT_NAME } from "../../reducers/PageContent"

const setName = (value) => {

    return {type: SET_DASHBOARD_PAGECONTENT_NAME, payload: {name: value }}

}

export { setName }
