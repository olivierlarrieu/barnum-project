import React from 'react';
import PropTypes from 'prop-types';


/** the PageContent component */
class PageContent extends React.Component {

    render() {

        return (

            <div className={"PageContentComponent"}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the PageContent component propTypes for typechecking */
PageContent.propTypes = {

}

export default PageContent
