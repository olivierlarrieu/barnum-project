import React from 'react';
import PropTypes from 'prop-types';


/** the Menu component */
class Menu extends React.Component {

    render() {

        return (

            <div className={`MenuComponent ${this.props.isOpen ? "": "close"}`}>

            { this.props.children }

            </div>

        )
    
    }

}

/** the Menu component propTypes for typechecking */
Menu.propTypes = {

}

export default Menu
