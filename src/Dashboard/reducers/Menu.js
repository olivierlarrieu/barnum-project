import { setState } from '../libs'

//constants
const SET_DASHBOARD_MENU_NAME = "SET_DASHBOARD_MENU_NAME"
const SET_DASHBOARD_MENU_ISOPEN = "SET_DASHBOARD_MENU_ISOPEN"


const MenuState = {
    //props
    name: "Menu",
    isOpen: "true",

}

const MenuReducer = (state = MenuState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_DASHBOARD_MENU_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        case SET_DASHBOARD_MENU_ISOPEN:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { MenuState, MenuReducer, SET_DASHBOARD_MENU_NAME, SET_DASHBOARD_MENU_ISOPEN }
