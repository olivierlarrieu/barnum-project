import { setState } from '../libs'

//constants
const SET_DASHBOARD_PAGECONTENT_NAME = "SET_DASHBOARD_PAGECONTENT_NAME"


const PageContentState = {
    //props
    name: "PageContent",

}

const PageContentReducer = (state = PageContentState, action) => {

    let newState = {}
    switch (action.type) {
        //switch

        case SET_DASHBOARD_PAGECONTENT_NAME:
            newState = setState(state, { ...action.payload })
            return newState

        default:
            return state

    }

}


export { PageContentState, PageContentReducer, SET_DASHBOARD_PAGECONTENT_NAME }
