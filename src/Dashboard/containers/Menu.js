import React from 'react';
import PropTypes from 'prop-types';
import Mousetrap from 'mousetrap'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import MenuComponent from '../components/Menu';
//actions
import { setName, setIsOpen } from "../actions/Menu"


const MenuItem = ({to, currentLocation, title, }) => (

    <Link to={to} className={`${ currentLocation === to ? "selected":""}`}>
    
        <div >{title}</div>
    
    </Link>

)

/** the Menu component */
class Menu extends React.Component {

    componentWillMount() {

        this.props.setName(this.props.location.pathname)

        this.unlisten = this.props.history.listen((location, action) => {

          this.props.setName(location.pathname)

        })

        Mousetrap.bind('ctrl+x', (e) => {
        
            this.props.setIsOpen(!this.props.isOpen)
        
        })

    }

    componentWillUnmount() {

        this.unlisten();

    }

    render() {

        return (

            <MenuComponent isOpen={this.props.isOpen}>
            <div className="MenuHeader"></div>
            {
                this.props.resources.map((resource, index) => {

                    return <MenuItem key={index} to={resource.path} currentLocation={this.props.name} title={resource.name}/>

                })
            }
            </MenuComponent>

        )
    
    }

}

/** the Menu component propTypes for typechecking */
Menu.propTypes = {

}

//-----------------------------------------------------------------------------
/** the Menu component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Dashboard_Menu.name,
        isOpen: state.Dashboard_Menu.isOpen,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
        setIsOpen: (value) => { dispatch(setIsOpen(value)) },
    }
}


const MenuContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(withRouter(Menu))
//-----------------------------------------------------------------------------

export { Menu, MenuContainer }
