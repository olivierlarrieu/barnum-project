import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PageContentComponent from '../components/PageContent';
//actions
import { setName } from "../actions/PageContent"

/** the PageContent component */
class PageContent extends React.Component {

    render() {

        return (

            <PageContentComponent>
            { this.props.children }
            </PageContentComponent>
            
        )
    
    }

}

/** the PageContent component propTypes for typechecking */
PageContent.propTypes = {

}

//-----------------------------------------------------------------------------
/** the PageContent component redux connect mandatories functions */
const mapStateToProps = (state, ownProps) => {
    return {
        //mapStateToProps
        name: state.Dashboard_PageContent.name,
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        //mapDispatchToProps
        setName: (value) => { dispatch(setName(value)) },
    }
}


const PageContentContainer = connect (
    mapStateToProps,
    mapDispatchToProps
)(PageContent)
//-----------------------------------------------------------------------------

export { PageContent, PageContentContainer }
