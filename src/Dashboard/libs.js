const setState = (state, obj) => Object.assign({}, state, obj)

export { setState }
