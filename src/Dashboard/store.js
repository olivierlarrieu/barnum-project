import {  
  combineReducers,
  createStore,
} from 'redux';

//imports
import { MenuReducer } from 'Dashboard/reducers/Menu'
import { PageContentReducer } from 'Dashboard/reducers/PageContent'

const RootReducer = {
  //reducers
  Dashboard_Menu: MenuReducer,
  Dashboard_PageContent: PageContentReducer,
}

const createReducer = (store={}, loadedReducer={}) => {

  store.LOADED_REDUCERS = {...store.LOADED_REDUCERS, ...loadedReducer}
  let newObject = {...RootReducer, ...store.LOADED_REDUCERS}
  let newReducer = combineReducers(
          newObject
  )
  return newReducer
}

const store = createStore(createReducer(), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
store.LOADED_REDUCERS = {}


export { createReducer, RootReducer }
export default store
