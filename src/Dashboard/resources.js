import React from 'react';
import { Switch, Route } from 'react-router-dom'
import LoadableComponent from './components/Loadable'
const Icon = (props) => <div>.{props.children}</div>


const Resources = [
  {
    name: 'Home',
    icon: Icon,
    path: '/home',
    component: LoadableComponent(() => { return import('Home/app.route') })
  },
  {
    name: 'Demo',
    icon: Icon,
    path: '/demo',
    component: LoadableComponent(() => { return import('Demo/app.resource') })
  },


]


const ResourcesProvider = () => {
    return (

        <Switch>
        {
            Resources.map((resource, index) => {
                return <Route key={index} path={resource.path} component={resource.component}/>
            })
        }
        </Switch>

    )
}

export default Resources
export { ResourcesProvider }
