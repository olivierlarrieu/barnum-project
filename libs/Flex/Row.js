import React from 'react';


const FlexRow = (props) => (

    <div {...props} className={`row ${props.className}`} style={{...props.style}}>

        {props.children}

    </div>

)

export default FlexRow
