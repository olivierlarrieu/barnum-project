import React from 'react';


const FlexCol = (props) => (

    <div
        {...props}
        className={`col ${props.xs ? "col-xs-" + props.xs : ""} ${props.sm ? "col-sm-" + props.sm : ""} ${props.md ? "col-md-" + props.md : ""} ${props.lg ? "col-lg-" + props.lg : ""} ${props.className}`}
        style={{...props.style}}
    >
        {props.children}

    </div>

)

export default FlexCol
